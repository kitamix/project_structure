﻿using ProjectStructure.Shared.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ProjectStructure.DAL.Repositories
{
    public sealed class TaskRepository : IRepository<Task>
    {
        private List<Task> _tasks = new();

        public void RefreshData(IEnumerable<Task> data)
        {
            if (data.Any())
                _tasks = data.ToList();
        }

        public void Create(Task item)
        {
            _tasks.Add(item);
        }

        public void Delete(int id)
        {
            _tasks.Remove(_tasks.FirstOrDefault(t => t.Id == id));
        }

        public IEnumerable<Task> Get()
        {
            return _tasks;
        }

        public Task Get(int id)
        {
            return _tasks.FirstOrDefault(t => t.Id == id);
        }

        public void Update(int id, Task item)
        {
            var itemId = _tasks.IndexOf(_tasks.FirstOrDefault(t => t.Id == id));
            _tasks[itemId] = item;
            _tasks[itemId].Id = id;
        }
    }
}