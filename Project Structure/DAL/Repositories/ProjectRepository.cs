﻿using ProjectStructure.Shared.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructure.DAL.Repositories
{
    public sealed class ProjectRepository : IRepository<Project>
    {
        private List<Project> _projects = new();


        public void RefreshData(IEnumerable<Project> data)
        {
            if (data.Any())
                _projects = data.ToList();
        }
        public void Create(Project item)
        {
            _projects.Add(item);
        }

        public void Delete(int id)
        {
            _projects.Remove(_projects.FirstOrDefault(p => p.Id == id));
        }

        public IEnumerable<Project> Get()
        {
            return _projects;
        }

        public Project Get(int id)
        {
            return _projects.FirstOrDefault(p => p.Id == id);
        }

        public void Update(int id, Project item)
        {
            var itemId = _projects.IndexOf(_projects.FirstOrDefault(p => p.Id == id));
            _projects[itemId] = item;
            _projects[itemId].Id = id;
        }
    }
}