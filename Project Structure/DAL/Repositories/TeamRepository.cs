﻿using ProjectStructure.Shared.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ProjectStructure.DAL.Repositories
{
    public sealed class TeamRepository : IRepository<Team>
    {
        private List<Team> _teams = new();

        public void RefreshData(IEnumerable<Team> data)
        {
            if (data.Any())
                _teams = data.ToList();
        }
        public void Create(Team item)
        {
            _teams.Add(item);
        }

        public void Delete(int id)
        {
            _teams.Remove(_teams.FirstOrDefault(p => p.Id == id));
        }

        public IEnumerable<Team> Get()
        {
            return _teams;
        }

        public Team Get(int id)
        {
            return _teams.FirstOrDefault(p => p.Id == id);
        }

        public void Update(int id, Team item)
        {
            var itemId = _teams.IndexOf(_teams.FirstOrDefault(p => p.Id == id));
            _teams[itemId] = item;
            _teams[itemId].Id = id;
        }
    }
}