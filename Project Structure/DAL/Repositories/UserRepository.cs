﻿using ProjectStructure.Shared.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ProjectStructure.DAL.Repositories
{
    public sealed class UserRepository : IRepository<User>
    {
        private List<User> _users = new();

        public void RefreshData(IEnumerable<User> data)
        {
            if (data.Any())
                _users = data.ToList();
        }
        public void Create(User item)
        {
            _users.Add(item);
        }

        public void Delete(int id)
        {
            _users.Remove(_users.FirstOrDefault(p => p.Id == id));
        }

        public IEnumerable<User> Get()
        {
            return _users;
        }

        public User Get(int id)
        {
            return _users.FirstOrDefault(p => p.Id == id);
        }

        public void Update(int id, User item)
        {
            var itemId = _users.IndexOf(_users.FirstOrDefault(p => p.Id == id));
            _users[itemId] = item;
            _users[itemId].Id = id;
        }
    }
}