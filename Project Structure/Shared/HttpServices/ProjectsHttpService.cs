﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectStructure.Shared.Models;
using ProjectStructure.Shared.Abstract;

namespace ProjectStructure.Shared.HttpServices
{
    public class ProjectsHttpService
    {
        private readonly HttpClient _httpClient = new();

        public ProjectsHttpService()
        {
            _httpClient.BaseAddress = new Uri($"{Settings.BaseAddress}/Projects/");
        }

        public async Task<List<Project>> GetAllProjects()
        {
            var content = await _httpClient.GetStringAsync("");
            return JsonConvert.DeserializeObject<List<Project>>(content);
        }

        public async Task<Project> GetProjectById(int id)
        {
            var content = await _httpClient.GetStringAsync($"{id}");
            return JsonConvert.DeserializeObject<Project>(content);
        }
    }
}