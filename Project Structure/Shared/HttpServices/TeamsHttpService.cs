﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using ProjectStructure.Shared.Models;
using ProjectStructure.Shared.Abstract;
using Newtonsoft.Json;

namespace ProjectStructure.Shared.HttpServices
{
    public class TeamsHttpService
    {
        private readonly HttpClient _httpClient = new();

        public TeamsHttpService()
        {
            _httpClient.BaseAddress = new Uri($"{Settings.BaseAddress}/Teams/");
        }

        public async Task<List<Team>> GetAllTeams()
        {
            var content = await _httpClient.GetStringAsync("");
            return JsonConvert.DeserializeObject<List<Team>>(content);
        }

        public async Task<Team> GetTeamById(int id)
        {
            var content = await _httpClient.GetStringAsync($"{id}");
            return JsonConvert.DeserializeObject<Team>(content);
        }
    }
}