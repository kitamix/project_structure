﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using ProjectStructure.Shared.Models;
using ProjectStructure.Shared.Abstract;
using Newtonsoft.Json;

namespace ProjectStructure.Shared.HttpServices
{
    public class TasksHttpService
    {
        private readonly HttpClient _httpClient = new();

        public TasksHttpService()
        {
            _httpClient.BaseAddress = new Uri($"{Settings.BaseAddress}/Tasks/");
        }

        public async System.Threading.Tasks.Task<List<Task>> GetAllTasks()
        {
            var content = await _httpClient.GetStringAsync("");
            return JsonConvert.DeserializeObject<List<Task>>(content);
        }

        public async System.Threading.Tasks.Task<Task> GetTaskById(int id)
        {
            var content = await _httpClient.GetStringAsync($"{id}");
            return JsonConvert.DeserializeObject<Task>(content);
        }
    }
}