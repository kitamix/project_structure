﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectStructure.Shared.Models
{
    public class User
    {
        public int Id { get; set; }

        [NotMapped] 
        public Team Team { get; set; }

        [NotMapped] 
        public List<Task> Tasks { get; set; }

        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
    }
}