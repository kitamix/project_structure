﻿namespace ProjectStructure.Shared.Models
{
    public enum TaskState
    {
        First = 0,
        Second = 1,
        Third = 2,
        Fourth = 3
    }
}