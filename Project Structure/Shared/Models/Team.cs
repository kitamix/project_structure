﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectStructure.Shared.Models
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CreatedAt { get; set; }

        [NotMapped] 
        public List<User> Users { get; set; }
    }
}