﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using ProjectStructure.Shared.Models;

namespace ProjectStructure.Shared.Models
{
    public class Project
    {
        public int Id { get; set; }

        [NotMapped] 
        public User Author { get; set; }

        public int AuthorId { get; set; }

        [NotMapped] 
        public Team Team { get; set; }

        public int? TeamId { get; set; }

        [NotMapped] 
        public List<Task> Tasks { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}