﻿using ProjectStructure.Shared.Models;

namespace ProjectStructure.Shared.DTOs
{
    public class UserAdditionalInfoDTO
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int TotalTasksCount { get; set; }
        public int TotalUncompletedAndCanceledTasks { get; set; }
        public Task LongestTask { get; set; }
    }
}