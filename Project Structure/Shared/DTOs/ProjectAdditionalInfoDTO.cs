﻿using ProjectStructure.Shared.Models;

namespace ProjectStructure.Shared.DTOs
{
    public class ProjectAdditionalInfoDTO
    {
        public Project Project { get; set; }
        public Task LongestTaskByDescription { get; set; }
        public Task ShortestTaskByName { get; set; }
        public int TotalTeamCount { get; set; }
    }
}