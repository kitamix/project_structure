﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Shared.DTOs;
using ProjectStructure.BL.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserService _userService;

        public UsersController(
            UserService userService
        )
        {
            _userService = userService;
        }

        [HttpPost]
        public void Create([FromBody] UserDTO userDTO)
        {
            _userService.Create(userDTO);
        }
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _userService.Delete(id);
        }
        [HttpGet]
        public IEnumerable<UserDTO> Get()
        {
            return _userService.Get();

        }
        [HttpGet("{id}")]
        public UserDTO Get(int id)
        {
            return _userService.Get(id);
        }

        [HttpPut("{id}")]
        public void Update(int id, [FromBody] UserDTO userDTO)
        {
            _userService.Update(id, userDTO);
        }
    }
}
