﻿using System;
using ProjectStructure.Shared.HttpServices;

namespace ProjectStructure.ConsoleMenu
{
    public static class Menu
    {
        private static readonly LinqHttpService LinqService = new();
        public static void Start()
        {
            Console.Clear();
            Console.WriteLine("---###MENU###---");
            Console.WriteLine("1. Count tasks of current user");
            Console.WriteLine("2. Get all tasks of user with name length < 45");
            Console.WriteLine("3. Get finished this year(2021) tasks of user");
            Console.WriteLine("4. Get users older than 10 sorted by registration date and grouped by team");
            Console.WriteLine("5. Sort by user first name and task name");
            Console.WriteLine("6. Analyze user's last tasks and project");
            Console.WriteLine("7. Analyze project's tasks and team");
            Console.WriteLine("8. Initialize default data");
            Console.WriteLine("9. Exit");
            Console.Write("\nWrite chosen menu option: ");

            if (!int.TryParse(Console.ReadLine(), out var menuKey))
            {
                Console.WriteLine("Wrong Input");
                Back();
            }

            Console.Clear();
            switch (menuKey)
            {
                case 1:
                    CountTasksOfCurrentUser();
                    break;
                case 2:
                    GetTasksOfCurrentUser();
                    break;
                case 3:
                    GetFinishedThisYearTasksOfCurrentUser();
                    break;
                case 4:
                    GetUsersOlderThanTenYearsOld();
                    break;
                case 5:
                    GetSortedUserByAscendingAndTasksByDescending();
                    break;
                case 6:
                    AnalyzeUserProjectsAndTasks();
                    break;
                case 7:
                    AnalyzeProjectTasksAndTeam();
                    break;
                case 8:
                    InitializeData();
                    break;
                case 9:
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Wrong menu option");
                    break;
            }

            Back();
        }


        public static void CountTasksOfCurrentUser()
        {
            Console.WriteLine("Write user ID: ");
            if (!int.TryParse(Console.ReadLine(), out var userId))
            {
                Console.WriteLine("Wrong input");
                return;
            }

            var result = LinqService.CountTasksOfCurrentUser(userId).GetAwaiter().GetResult();
            foreach (var (key, value) in result)
            {
                Console.WriteLine($"ProjectId: {key}, Tasks: {value}");
            }

            Back();
        }


        public static void GetTasksOfCurrentUser()
        {
            Console.WriteLine("Write user ID: ");

            if (!int.TryParse(Console.ReadLine(), out var userId))
            {
                Console.WriteLine("Wrong input");
                return;
            }

            var result = LinqService.GetTasksOfCurrentUser(userId).GetAwaiter().GetResult();

            foreach (var item in result)
            {
                Console.WriteLine($"\n\nName: {item.Name} \nDescription: {item.Description}");
            }

            Back();
        }


        public static void GetFinishedThisYearTasksOfCurrentUser()
        {
            Console.WriteLine("Write user ID: ");
            if (!int.TryParse(Console.ReadLine(), out var userId))
            {
                Console.WriteLine("Wrong input");
                return;
            }

            var result = LinqService.GetFinishedThisYearTasksOfCurrentUser(userId).GetAwaiter().GetResult();

            foreach (var (item1, item2) in result)
            {
                Console.WriteLine($"\n\nTask ID: {item1}\n Task Name: {item2}");
            }

            Back();
        }

        public static void GetUsersOlderThanTenYearsOld()
        {
            var result = LinqService.GetUsersOlderThanTenYearsOld().GetAwaiter().GetResult();

            foreach (var item in result)
            {
                Console.WriteLine($"\n\nTeam ID: {item.Id}, Team Name: {item.TeamName}. \nUsers:");
                foreach (var user in item.UsersOlderThanTenYearsOld)
                {
                    Console.WriteLine($"{user.FirstName} {user.LastName} ");
                }
            }

            Back();
        }


        public static void GetSortedUserByAscendingAndTasksByDescending()
        {
            var result = LinqService.GetSortedUserByAscendingAndTasksByDescending().GetAwaiter().GetResult();

            foreach (var item in result)
            {
                Console.WriteLine($"\n\nUser First Name: {item.UserName}. Tasks:");
                foreach (var task in item.SortedTasks)
                {
                    Console.WriteLine(task.Name);
                }
            }

            Back();
        }


        public static void AnalyzeUserProjectsAndTasks()
        {
            Console.WriteLine("Write user ID: ");
            if (!int.TryParse(Console.ReadLine(), out var userId))
            {
                Console.WriteLine("Wrong input");
                return;
            }

            var result = LinqService.AnalyzeUserProjectsAndTasks(userId).GetAwaiter().GetResult();
            if (result == null)
            {
                Console.WriteLine("Not Found");
                return;
            }

            Console.WriteLine($"\n\nUser: {result.User.FirstName} {result.User.LastName}");
            Console.WriteLine($"Last Project: {result.LastProject?.Name}");
            Console.WriteLine($"Total tasks count: {result.TotalTasksCount}");
            Console.WriteLine($"Uncompleted and canceled tasks count: {result.TotalUncompletedAndCanceledTasks}");
            Console.WriteLine($"Longest task: {result.LongestTask?.Name}");


            Back();
        }


        public static void AnalyzeProjectTasksAndTeam()
        {
            Console.WriteLine("Write project ID: ");
            if (!int.TryParse(Console.ReadLine(), out var projectId))
            {
                Console.WriteLine("Wrong input");
                return;
            }

            var result = LinqService.AnalyzeProjectTasksAndTeam(projectId).GetAwaiter().GetResult();
            Console.WriteLine($"\n\nProject: {result.Project?.Name}"); 
            Console.WriteLine($"Longest task by description: {result.LongestTaskByDescription?.Description}");
            Console.WriteLine($"Shortest task by name: {result.ShortestTaskByName?.Name}");
            Console.WriteLine($"Total team count where project description length > 20 or tasks count < 3: {result.TotalTeamCount}");
            Back();
        }


        public static void InitializeData()
        {
            LinqService.LoadTestData();
        }


        public static void Back()
        {
            Console.WriteLine("\nPress any key to go back...");
            Console.ReadKey();
            Start();
        }
    }
}