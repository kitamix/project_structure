﻿using AutoMapper;
using ProjectStructure.DAL;
using ProjectStructure.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.BL.Services.Abstract
{
    public abstract class BaseService<T> where T : class
    {
        private protected readonly UnitOfWork UnitOfWork;
        private protected readonly IMapper Mapper;

        public BaseService(UnitOfWork unitOfWork, IMapper mapper)
        {
            UnitOfWork = unitOfWork;
            Mapper = mapper;
        }
    }
}