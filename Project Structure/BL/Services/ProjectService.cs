﻿using AutoMapper;
using ProjectStructure.BL.Services.Abstract;
using ProjectStructure.Shared.DTOs;
using ProjectStructure.DAL;
using ProjectStructure.Shared.Models;
using ProjectStructure.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.BL.Services
{
    public sealed class ProjectService : BaseService<Project>
    {
        public ProjectService(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) { }

        public void Create(ProjectDTO projectDTO)
        {
            var project = Mapper.Map<Project>(projectDTO);
            UnitOfWork.Projects.Create(project);
            UnitOfWork.SaveChanges();
        }
        public void Delete(int id)
        {
            UnitOfWork.Projects.Delete(id);
            UnitOfWork.SaveChanges();
        }

        public IEnumerable<ProjectDTO> Get()
        {
            return Mapper.Map<IEnumerable<ProjectDTO>>(UnitOfWork.Projects.Get());
        }

        public ProjectDTO Get(int id)
        {
            return Mapper.Map<ProjectDTO>(UnitOfWork.Projects.Get(id));
        }

        public void Update(int id, ProjectDTO projectDTO)
        {
            var project = Mapper.Map<Project>(projectDTO);
            UnitOfWork.Projects.Update(id, project);
            UnitOfWork.SaveChanges();
        }


    }
}