﻿using AutoMapper;
using ProjectStructure.BL.Services.Abstract;
using ProjectStructure.Shared.DTOs;
using ProjectStructure.DAL;
using ProjectStructure.Shared.Models;
using ProjectStructure.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.BL.Services
{
    public sealed class TeamService : BaseService<Team>
    {
        public TeamService(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) { }

        public void Create(TeamDTO teamDTO)
        {
            var team = Mapper.Map<Team>(teamDTO);
            UnitOfWork.Teams.Create(team);
            UnitOfWork.SaveChanges();
        }
        public void Delete(int id)
        {
            UnitOfWork.Teams.Delete(id);
            UnitOfWork.SaveChanges();
        }

        public IEnumerable<TeamDTO> Get()
        {
            return Mapper.Map<IEnumerable<TeamDTO>>(UnitOfWork.Teams.Get());
        }

        public TeamDTO Get(int id)
        {
            return Mapper.Map<TeamDTO>(UnitOfWork.Teams.Get(id));
        }

        public void Update(int id, TeamDTO teamDTO)
        {
            var team = Mapper.Map<Team>(teamDTO);
            UnitOfWork.Teams.Update(id, team);
            UnitOfWork.SaveChanges();
        }
    }
}