﻿using AutoMapper;
using ProjectStructure.BL.Services.Abstract;
using ProjectStructure.Shared.DTOs;
using ProjectStructure.DAL;
using ProjectStructure.Shared.Models;
using ProjectStructure.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.BL.Services
{
    public sealed class UserService : BaseService<User>
    {
        public UserService(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) { }

        public void Create(UserDTO userDTO)
        {
            var user = Mapper.Map<User>(userDTO);
            UnitOfWork.Users.Create(user);
            UnitOfWork.SaveChanges();
        }
        public void Delete(int id)
        {
            UnitOfWork.Users.Delete(id);
            UnitOfWork.SaveChanges();
        }

        public IEnumerable<UserDTO> Get()
        {
            return Mapper.Map<IEnumerable<UserDTO>>(UnitOfWork.Users.Get());
        }

        public UserDTO Get(int id)
        {
            return Mapper.Map<UserDTO>(UnitOfWork.Users.Get(id));
        }

        public void Update(int id, UserDTO userDTO)
        {
            var user = Mapper.Map<User>(userDTO);
            UnitOfWork.Users.Update(id, user);
            UnitOfWork.SaveChanges();
        }
    }
}