﻿using AutoMapper;
using ProjectStructure.BL.Services.Abstract;
using ProjectStructure.Shared.DTOs;
using ProjectStructure.DAL;
using ProjectStructure.Shared.Models;
using ProjectStructure.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructure.BL.Services
{
    public sealed class LinqService
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public LinqService
        (
            UnitOfWork unitOfWork,
            IMapper mapper
        )
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public IDictionary<int, int> CountTasksOfCurrentUser(int userId)
        {
            var query = _unitOfWork.Projects.Get()
                .Where(p => p.AuthorId == userId)
                .ToDictionary(k => k.Id, v => v.Tasks.Count);
            return query;
        }
        public List<TaskDTO> GetTasksOfCurrentUser(int userId)
        {
            var query = _unitOfWork.Tasks.Get()
                .Where(t => t.PerformerId == userId && t.Name.Length < 45)
                .ToList();
            return _mapper.Map<List<TaskDTO>>(query);
        }
        public List<Tuple<int, string>> GetFinishedThisYearTasksOfCurrentUser(int userId)
        {
            var query = _unitOfWork.Tasks.Get()
                .Where(t => t.PerformerId == userId
                            && t.FinishedAt.HasValue
                            && t.FinishedAt.Value.Year == DateTime.Now.AddYears(-1).Year)
                .Select((task) => new Tuple<int, string>(task.Id, task.Name))
                .ToList();
            return query;
        }
        public IEnumerable<UsersOlderThanTenYearsOldDTO> GetUsersOlderThanTenYearsOld()
        {
            var query = _unitOfWork.Teams.Get()
                .Select(t => new UsersOlderThanTenYearsOldDTO
                {
                    Id = t.Id,
                    TeamName = t.Name,
                    UsersOlderThanTenYearsOld = _mapper
                        .Map<IEnumerable<UserDTO>>(t.Users.Where(u => DateTime.Now.Year - u.BirthDay.Year > 10))
                        .OrderByDescending(u => u.RegisteredAt)
                });
            return query;
        }
        public IOrderedEnumerable<SortedUserByAscendingAndTasksByDescending>
            GetSortedUserByAscendingAndTasksByDescending()
        {
            var query = _unitOfWork.Users.Get()
                .Select(u => new SortedUserByAscendingAndTasksByDescending
                {
                    UserName = u.FirstName,
                    SortedTasks = _mapper.Map<List<TaskDTO>>(u.Tasks.OrderByDescending(t => t.Name.Length).ToList())
                })
                .OrderBy(x => x.UserName);
            return query;
        }
        public UserAdditionalInfoDTO AnalyzeUserProjectsAndTasks(int userId)
        {
            var query = _unitOfWork.Users.Get()
                .Where(u => u.Id == userId)
                .Select(u => new UserAdditionalInfoDTO
                {
                    User = u,
                    LastProject = _unitOfWork.Projects.Get()
                        .Where(p => p.AuthorId == userId)
                        .OrderByDescending(p => p.CreatedAt)
                        .FirstOrDefault(),
                    TotalTasksCount = _unitOfWork.Projects.Get()
                        .Where(p => p.AuthorId == userId)
                        .OrderByDescending(p => p.CreatedAt)
                        .FirstOrDefault()
                        .Tasks
                        .Count,
                    TotalUncompletedAndCanceledTasks = u.Tasks
                        .Where(t => !t.FinishedAt.HasValue)
                        .Count(),
                    LongestTask = _unitOfWork.Projects.Get().Where(p => p.AuthorId == userId)
                        .OrderByDescending(p => p.CreatedAt).FirstOrDefault().Tasks
                        .Where(t => t.FinishedAt.HasValue)
                        .OrderByDescending(t => t.FinishedAt.Value.Ticks - t.CreatedAt.Ticks)
                        .FirstOrDefault()
                })
                .First();
            return query;
        }
        public ProjectAdditionalInfoDTO AnalyzeProjectTasksAndTeam(int projectId)
        {
            var query = _unitOfWork.Projects.Get()
                .Where(p => p.Id == projectId)
                .Select(p => new ProjectAdditionalInfoDTO
                {
                    Project = p,
                    LongestTaskByDescription = p.Tasks
                        .OrderByDescending(t => t.Description.Length)
                        .FirstOrDefault(),
                    ShortestTaskByName = p.Tasks
                        .OrderBy(t => t.Name.Length)
                        .FirstOrDefault(),
                    TotalTeamCount = p.Description.Length > 20 || p.Tasks.Count < 3 ? p.Team.Users.Count : 0
                })
                .First();
            return query;
        }

        public void LoadTestData()
        {
            _unitOfWork.LoadTestData();
        }
    }
}