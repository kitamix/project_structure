﻿using AutoMapper;
using ProjectStructure.BL.Services.Abstract;
using ProjectStructure.Shared.DTOs;
using ProjectStructure.DAL;
using ProjectStructure.Shared.Models;
using ProjectStructure.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.BL.Services
{
    public sealed class TaskService : BaseService<Task>
    {
        public TaskService(UnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) { }

        public void Create(TaskDTO taskDTO)
        {
            var task = Mapper.Map<Task>(taskDTO);
            UnitOfWork.Tasks.Create(task);
            UnitOfWork.SaveChanges();
        }
        public void Delete(int id)
        {
            UnitOfWork.Tasks.Delete(id);
            UnitOfWork.SaveChanges();
        }

        public IEnumerable<TaskDTO> Get()
        {
            return Mapper.Map<IEnumerable<TaskDTO>>(UnitOfWork.Tasks.Get());
        }

        public TaskDTO Get(int id)
        {
            return Mapper.Map<TaskDTO>(UnitOfWork.Tasks.Get(id));
        }

        public void Update(int id, TaskDTO taskDTO)
        {
            var task = Mapper.Map<Task>(taskDTO);
            UnitOfWork.Tasks.Update(id, task);
            UnitOfWork.SaveChanges();
        }
    }
}